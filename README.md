# irods-wur-microservices-cmake

Updated CMakeLists.txt for [wur-microservices](https://github.com/chStaiger/wur-microservices)

Changes: fixed cxx flags, libs and includes, rpaths and rpm pkg requires

See [irods-microservices](https://gitlab.com/mkorthof/irods-microservices)

